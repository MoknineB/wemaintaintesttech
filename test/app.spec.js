import pactum from 'pactum'

//Normal case
it('ping', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/ping')
    .expectStatus(200);
});

it('search RadioHead Band/4 Non Blondes', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/search')
    .withQueryParams('bandIds', '[857,3]')
    .withHeaders('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQyLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDE4OTY0NTh9.Fm03RASssKqS8ConHP8uh7bUXsESxZPaXusQRX28k3Y')
    .expectStatus(200)
    .expectJson({
        "success": true,
        "result": [
            {
                "band": "4 Non Blondes",
                "location": "Palace Theatre, Melbourne, VIC, Australia",
                "date": 1203287610403,
                "latitude": -37.8673546,
                "longitude": 144.976156
            },
            {
                "band": "Radiohead",
                "location": "Circo Volador, Mexico City, Mexico",
                "date": 1207402337111,
                "latitude": 19.4060255,
                "longitude": -99.1256083
            },
            {
                "band": "4 Non Blondes",
                "location": "Rock School Barbey, Bordeaux, France",
                "date": 1220105857448,
                "latitude": 44.8269694,
                "longitude": -0.5632283
            },
            {
                "band": "Radiohead",
                "location": "The Tractor Tavern, Seattle, WA, US",
                "date": 1248076259593,
                "latitude": 47.6657705,
                "longitude": -122.3826454
            },
            {
                "band": "4 Non Blondes",
                "location": "NYCB Theatre at Westbury, New York, NY, US",
                "date": 1254391428170,
                "latitude": 40.7733696,
                "longitude": -73.5589498
            },
            {
                "band": "Radiohead",
                "location": "Ampere, Munich, Germany",
                "date": 1255956398796,
                "latitude": 48.1329363,
                "longitude": 11.5894601
            },
            {
                "band": "Radiohead",
                "location": "B.B. King Blues Club & Grill, New York, NY, US",
                "date": 1270518379349,
                "latitude": 40.7568984,
                "longitude": -73.98846139999999
            },
            {
                "band": "4 Non Blondes",
                "location": "The Tabernacle, Atlanta, GA, US",
                "date": 1276686820148,
                "latitude": 33.7586647,
                "longitude": -84.3914487
            },
            {
                "band": "4 Non Blondes",
                "location": "El Lokal, Zürich, Switzerland",
                "date": 1281645744932,
                "latitude": 47.374641,
                "longitude": 8.533891299999999
            },
            {
                "band": "Radiohead",
                "location": "Disco Volante, Oakland, CA, US",
                "date": 1300243581368,
                "latitude": 37.8031731,
                "longitude": -122.2686268
            },
            {
                "band": "Radiohead",
                "location": "Stork Club, Oakland, CA, US",
                "date": 1307964880843,
                "latitude": 37.8131387,
                "longitude": -122.2683684
            },
            {
                "band": "Radiohead",
                "location": "Sports Palace / Palacio de los Deportes, Mexico City, Mexico",
                "date": 1316861295039,
                "latitude": 19.4053359,
                "longitude": -99.09986459999999
            },
            {
                "band": "4 Non Blondes",
                "location": "Sound Academy, Toronto, ON, Canada",
                "date": 1359976491899,
                "latitude": 43.63967479999999,
                "longitude": -79.3535794
            },
            {
                "band": "Radiohead",
                "location": "Fuse, Brussels, Belgium",
                "date": 1368265881257,
                "latitude": 50.835332,
                "longitude": 4.344856
            },
            {
                "band": "Radiohead",
                "location": "Oakland Stadium, Oakland, CA, US",
                "date": 1375809398628,
                "latitude": 37.7515946,
                "longitude": -122.2005458
            },
            {
                "band": "4 Non Blondes",
                "location": "KOKO, London, UK",
                "date": 1388269535496,
                "latitude": 51.5347377,
                "longitude": -0.1382573
            },
            {
                "band": "4 Non Blondes",
                "location": "Constellation Room at the Observatory, Los Angeles, CA, US",
                "date": 1400330477132,
                "latitude": 33.6997278,
                "longitude": -117.9175687
            },
            {
                "band": "Radiohead",
                "location": "Maple Leaf Gardens, Toronto, ON, Canada",
                "date": 1421734425265,
                "latitude": 43.6621838,
                "longitude": -79.38033089999999
            },
            {
                "band": "4 Non Blondes",
                "location": "Eventim Apollo, London, UK",
                "date": 1424799938529,
                "latitude": 51.4910136,
                "longitude": -0.2246563
            },
            {
                "band": "4 Non Blondes",
                "location": "El Lokal, Zürich, Switzerland",
                "date": 1434289031979,
                "latitude": 47.374641,
                "longitude": 8.533891299999999
            },
            {
                "band": "Radiohead",
                "location": "Electrowerkz, London, UK",
                "date": 1437378826279,
                "latitude": 51.5325178,
                "longitude": -0.1050021
            },
            {
                "band": "Radiohead",
                "location": "Club Anton, Oakland, CA, US",
                "date": 1439289021485,
                "latitude": 37.7967999,
                "longitude": -122.2754412
            },
            {
                "band": "4 Non Blondes",
                "location": "3Arena, Dublin, Ireland",
                "date": 1449024010834,
                "latitude": 53.3474963,
                "longitude": -6.2285078
            },
            {
                "band": "Radiohead",
                "location": "C-Club, Berlin, Germany",
                "date": 1464215813168,
                "latitude": 52.484733,
                "longitude": 13.391426
            },
            {
                "band": "Radiohead",
                "location": "Magasin 4, Brussels, Belgium",
                "date": 1466343225977,
                "latitude": 50.864014,
                "longitude": 4.349808100000001
            },
            {
                "band": "4 Non Blondes",
                "location": "La Flèche d'Or, Paris, France",
                "date": 1487722036494,
                "latitude": 48.859433,
                "longitude": 2.402906
            },
            {
                "band": "4 Non Blondes",
                "location": "La Fourmi, Limoges, France",
                "date": 1506266927971,
                "latitude": 45.85046639999999,
                "longitude": 1.223815
            },
            {
                "band": "4 Non Blondes",
                "location": "Royal Theatre Carré (Koninklijk Theater Carré), Amsterdam, Netherlands",
                "date": 1515150473445,
                "latitude": 52.3623872,
                "longitude": 4.9042637
            },
            {
                "band": "Radiohead",
                "location": "Cato's Ale House, Oakland, CA, US",
                "date": 1524549226886,
                "latitude": 37.8249501,
                "longitude": -122.2545726
            },
            {
                "band": "Radiohead",
                "location": "New Morning, Paris, France",
                "date": 1530748301171,
                "latitude": 48.8730851,
                "longitude": 2.3533545
            },
            {
                "band": "Radiohead",
                "location": "Troubadour, Los Angeles, CA, US",
                "date": 1547095238785,
                "latitude": 34.0816229,
                "longitude": -118.3893577
            },
            {
                "band": "4 Non Blondes",
                "location": "Sony Centre for the Performing Arts, Toronto, ON, Canada",
                "date": 1550266041260,
                "latitude": 43.6466723,
                "longitude": -79.3760205
            },
            {
                "band": "Radiohead",
                "location": "I.BOAT, Bordeaux, France",
                "date": 1556209175568,
                "latitude": 44.8658875,
                "longitude": -0.5590402999999999
            },
            {
                "band": "4 Non Blondes",
                "location": "Paramount Theatre, Oakland, CA, US",
                "date": 1566048590758,
                "latitude": 37.8097055,
                "longitude": -122.2682219
            },
            {
                "band": "Radiohead",
                "location": "Point Ephémère, Paris, France",
                "date": 1569531810650,
                "latitude": 48.8814422,
                "longitude": 2.3684356
            }
        ]
    });
});

it('search RadioHead and 4 Non Blondes Band and geographique position', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/search')
    .withQueryParams('bandIds', '[857,3]')
    .withQueryParams('latitude','48.8814422')
    .withQueryParams('longitude','2.3684356')
    .withQueryParams('radius','10')
    .withHeaders('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQyLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDE4OTY0NTh9.Fm03RASssKqS8ConHP8uh7bUXsESxZPaXusQRX28k3Y')
    .expectStatus(200)
    .expectJson({
        "success": true,
        "result": [
            {
                "band": "4 Non Blondes",
                "location": "La Flèche d'Or, Paris, France",
                "date": 1487722036494,
                "latitude": 48.859433,
                "longitude": 2.402906
            },
            {
                "band": "Radiohead",
                "location": "New Morning, Paris, France",
                "date": 1530748301171,
                "latitude": 48.8730851,
                "longitude": 2.3533545
            },
            {
                "band": "Radiohead",
                "location": "Point Ephémère, Paris, France",
                "date": 1569531810650,
                "latitude": 48.8814422,
                "longitude": 2.3684356
            }
        ]
    });
});

it('search by lat, long and radius of 10 kms', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/search')
    .withQueryParams('latitude','48.8814422')
    .withQueryParams('longitude','2.3684356')
    .withQueryParams('radius','10')
    .withHeaders('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQyLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDE4OTY0NTh9.Fm03RASssKqS8ConHP8uh7bUXsESxZPaXusQRX28k3Y')
    .expectStatus(200)
    .expectJson({
        "success": true,
        "result": [
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Maschinenhaus i.d. KulturBrauerei, Berlin, Germany",
                "date": 1200386272178,
                "latitude": 52.5393856,
                "longitude": 13.4137106
            },
            {
                "band": "Everlast",
                "location": "Auditorio Nacional, Mexico City, Mexico",
                "date": 1200396182324,
                "latitude": 19.4248097,
                "longitude": -99.19492559999999
            },
            {
                "band": "Everlast",
                "location": "The Loft, The Chance Theater, New York, NY, US",
                "date": 1202247789984,
                "latitude": 41.7028034,
                "longitude": -73.92435449999999
            },
            {
                "band": "Christian Burns",
                "location": "Irvine Meadows Amphitheatre, Los Angeles, CA, US",
                "date": 1204520939796,
                "latitude": 34.1794169,
                "longitude": -118.385617
            },
            {
                "band": "Crooked Fingers",
                "location": "Cafe Rosenberg, Reykjavík, Iceland",
                "date": 1204603092606,
                "latitude": 64.14613489999999,
                "longitude": -21.9287967
            },
            {
                "band": "Christian Burns",
                "location": "El Lokal, Zürich, Switzerland",
                "date": 1205775591737,
                "latitude": 47.374641,
                "longitude": 8.533891299999999
            },
            {
                "band": "Black Veil Brides",
                "location": "Klub Gwarek, Krakow, Poland",
                "date": 1208610796662,
                "latitude": 50.0658875,
                "longitude": 19.9156862
            },
            {
                "band": "Black Box Recorder",
                "location": "O.co Coliseum, Oakland, CA, US",
                "date": 1209918249668,
                "latitude": 37.7515946,
                "longitude": -122.2005458
            },
            {
                "band": "Doves",
                "location": "Zénith de Limoges, Limoges, France",
                "date": 1213787970297,
                "latitude": 45.8622864,
                "longitude": 1.27935
            },
            {
                "band": "Black Box Recorder",
                "location": "Fremont Abbey Arts Center, Seattle, WA, US",
                "date": 1213797996388,
                "latitude": 47.6591888,
                "longitude": -122.3496822
            },
            {
                "band": "Crooked Fingers",
                "location": "Roundhouse, London, UK",
                "date": 1217647594915,
                "latitude": 51.5432454,
                "longitude": -0.151926
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Paradiso, Amsterdam, Netherlands",
                "date": 1218171102850,
                "latitude": 52.3621516,
                "longitude": 4.883806499999999
            },
            {
                "band": "Everlast",
                "location": "3Arena, Dublin, Ireland",
                "date": 1218489490143,
                "latitude": 53.3474963,
                "longitude": -6.2285078
            },
            {
                "band": "Dirty Three",
                "location": "Kex Hostel, Reykjavík, Iceland",
                "date": 1218954415471,
                "latitude": 64.14542399999999,
                "longitude": -21.919484
            },
            {
                "band": "8stops7",
                "location": "Rod Laver Arena, Melbourne, VIC, Australia",
                "date": 1219747927527,
                "latitude": -37.8216833,
                "longitude": 144.9785474
            },
            {
                "band": "Dave Matthews Band",
                "location": "The Roxy Theatre, Los Angeles, CA, US",
                "date": 1220130052341,
                "latitude": 34.0907813,
                "longitude": -118.388035
            },
            {
                "band": "Black Veil Brides",
                "location": "O2 Forum Kentish Town, London, UK",
                "date": 1220339760075,
                "latitude": 51.55219700000001,
                "longitude": -0.141969
            },
            {
                "band": "Crooked Fingers",
                "location": "El Plaza Condesa, Mexico City, Mexico",
                "date": 1223645025401,
                "latitude": 19.4132032,
                "longitude": -99.1719516
            },
            {
                "band": "Crooked Fingers",
                "location": "Trouw, Amsterdam, Netherlands",
                "date": 1224195092004,
                "latitude": 52.35438569999999,
                "longitude": 4.9127382
            },
            {
                "band": "Doves",
                "location": "Marquee Club - Wardour Street, London, UK",
                "date": 1225751642748,
                "latitude": 51.51421269999999,
                "longitude": -0.133794
            },
            {
                "band": "Dirty Three",
                "location": "Feierwerk / Orangehouse, Munich, Germany",
                "date": 1226069510185,
                "latitude": 48.1280689,
                "longitude": 11.5351389
            },
            {
                "band": "Black Box Recorder",
                "location": "Black Cat, Washington, DC, US",
                "date": 1227247482879,
                "latitude": 38.9145825,
                "longitude": -77.0317283
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "The Bell House, New York, NY, US",
                "date": 1227528791040,
                "latitude": 40.67351,
                "longitude": -73.991632
            },
            {
                "band": "Del Amitri",
                "location": "Neumos Crystal Ball Reading Room, Seattle, WA, US",
                "date": 1227796423362,
                "latitude": 47.613938,
                "longitude": -122.3196199
            },
            {
                "band": "Broken Social Scene",
                "location": "El Lokal, Zürich, Switzerland",
                "date": 1231719335829,
                "latitude": 47.374641,
                "longitude": 8.533891299999999
            },
            {
                "band": "Escape the Fate",
                "location": "Sala El Sol, Madrid, Spain",
                "date": 1231782437814,
                "latitude": 40.4190629,
                "longitude": -3.7016321
            },
            {
                "band": "Everlast",
                "location": "Privatclub, Berlin, Germany",
                "date": 1233279325151,
                "latitude": 52.50016,
                "longitude": 13.4351403
            },
            {
                "band": "Broken Social Scene",
                "location": "Royal Circus (Cirque Royal/Koninklijk Circus), Brussels, Belgium",
                "date": 1234267999738,
                "latitude": 50.8488477,
                "longitude": 4.366031599999999
            },
            {
                "band": "Escape the Fate",
                "location": "Madame JoJo's, London, UK",
                "date": 1236789258627,
                "latitude": 51.5124934,
                "longitude": -0.1338545
            },
            {
                "band": "Black Veil Brides",
                "location": "Marquee Club - Wardour Street, London, UK",
                "date": 1240314376369,
                "latitude": 51.51421269999999,
                "longitude": -0.133794
            },
            {
                "band": "Dirty Three",
                "location": "Olympiahalle, Munich, Germany",
                "date": 1240660195044,
                "latitude": 48.17546460000001,
                "longitude": 11.551797
            },
            {
                "band": "Dirty Three",
                "location": "Palalottomatica, Rome, Italy",
                "date": 1242744986915,
                "latitude": 41.8252534,
                "longitude": 12.4665412
            },
            {
                "band": "Dirty Three",
                "location": "Orion, Rome, Italy",
                "date": 1247475166013,
                "latitude": 41.8099128,
                "longitude": 12.5991784
            },
            {
                "band": "Every Avenue",
                "location": "Bitterzoet, Amsterdam, Netherlands",
                "date": 1248438891757,
                "latitude": 52.37739200000001,
                "longitude": 4.89413
            },
            {
                "band": "Everlast",
                "location": "Yoshi's Jazz Club, Oakland, Oakland, CA, US",
                "date": 1249695121605,
                "latitude": 37.796237,
                "longitude": -122.2785532
            },
            {
                "band": "Everlast",
                "location": "BT59, Begles, France",
                "date": 1253597186412,
                "latitude": 44.8132603,
                "longitude": -0.5525287
            },
            {
                "band": "Black Box Recorder",
                "location": "The Crocodile, Seattle, WA, US",
                "date": 1254706776388,
                "latitude": 47.6136087,
                "longitude": -122.3442873
            },
            {
                "band": "Christian Burns",
                "location": "The Basement, Sydney, NSW, Australia",
                "date": 1258408019855,
                "latitude": -33.86283,
                "longitude": 151.209916
            },
            {
                "band": "Black Box Recorder",
                "location": "Oracle Arena, Oakland, CA, US",
                "date": 1259927277564,
                "latitude": 37.7503024,
                "longitude": -122.2029607
            },
            {
                "band": "Dirty Three",
                "location": "Astoria, London, UK",
                "date": 1260520064386,
                "latitude": 51.5162105,
                "longitude": -0.1713663
            },
            {
                "band": "Daisy Chainsaw",
                "location": "Lunario del Auditorio Nacional, Mexico City, Mexico",
                "date": 1262471170103,
                "latitude": 19.4246215,
                "longitude": -99.1954026
            },
            {
                "band": "Dirty Three",
                "location": "Fonda Theatre, Los Angeles, CA, US",
                "date": 1262889311854,
                "latitude": 34.101486,
                "longitude": -118.3232871
            },
            {
                "band": "8stops7",
                "location": "Ippodromo Capannelle, Rome, Italy",
                "date": 1263387848817,
                "latitude": 41.8240481,
                "longitude": 12.5694535
            },
            {
                "band": "8stops7",
                "location": "The Knitting Factory - TriBeCa, New York, NY, US",
                "date": 1266779729460,
                "latitude": 40.7142193,
                "longitude": -73.9558328
            },
            {
                "band": "Dave Matthews Band",
                "location": "Sound Academy, Toronto, ON, Canada",
                "date": 1267220787978,
                "latitude": 43.63967479999999,
                "longitude": -79.3535794
            },
            {
                "band": "8stops7",
                "location": "Electric Ballroom, London, UK",
                "date": 1269700918309,
                "latitude": 51.5396971,
                "longitude": -0.1430325
            },
            {
                "band": "Daisy Chainsaw",
                "location": "The Airliner, Los Angeles, CA, US",
                "date": 1272292976063,
                "latitude": 34.0738477,
                "longitude": -118.2162662
            },
            {
                "band": "8stops7",
                "location": "Saint Vitus, New York, NY, US",
                "date": 1273223995952,
                "latitude": 40.7367758,
                "longitude": -73.95513369999999
            },
            {
                "band": "Escape the Fate",
                "location": "El Corazon, Seattle, WA, US",
                "date": 1275388525543,
                "latitude": 47.6187576,
                "longitude": -122.32934
            },
            {
                "band": "Crooked Fingers",
                "location": "Festsaal Kreuzberg, Berlin, Germany",
                "date": 1277409051132,
                "latitude": 52.4967039,
                "longitude": 13.4515843
            },
            {
                "band": "8stops7",
                "location": "Joy Eslava, Madrid, Spain",
                "date": 1278953993382,
                "latitude": 40.4171574,
                "longitude": -3.7065622
            },
            {
                "band": "Crooked Fingers",
                "location": "Underbelly Hoxton, London, UK",
                "date": 1279447428775,
                "latitude": 51.5280281,
                "longitude": -0.0817924
            },
            {
                "band": "Everlast",
                "location": "Harpa, Reykjavík, Iceland",
                "date": 1280376541175,
                "latitude": 64.1502464,
                "longitude": -21.9322804
            },
            {
                "band": "Escape the Fate",
                "location": "Kägelbanan Södra Teatern, Stockholm, Sweden",
                "date": 1281449806938,
                "latitude": 59.318424,
                "longitude": 18.074412
            },
            {
                "band": "Black Box Recorder",
                "location": "Södra Teaterns Stora Scen, Stockholm, Sweden",
                "date": 1285087104712,
                "latitude": 59.318424,
                "longitude": 18.074412
            },
            {
                "band": "Dirty Three",
                "location": "The Masquerade, Atlanta, GA, US",
                "date": 1285513556220,
                "latitude": 33.7515657,
                "longitude": -84.3897963
            },
            {
                "band": "Everlast",
                "location": "K17, Berlin, Germany",
                "date": 1285974215838,
                "latitude": 52.5183113,
                "longitude": 13.4717676
            },
            {
                "band": "Dirty Three",
                "location": "O2 Academy Brixton, London, UK",
                "date": 1286848993705,
                "latitude": 51.4656337,
                "longitude": -0.1149728
            },
            {
                "band": "Crooked Fingers",
                "location": "High Dive, Seattle, WA, US",
                "date": 1290899295423,
                "latitude": 47.6514909,
                "longitude": -122.351738
            },
            {
                "band": "Dirty Three",
                "location": "Olympia Theatre, Dublin, Ireland",
                "date": 1293417406786,
                "latitude": 53.3443127,
                "longitude": -6.2660797
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Nasti, Madrid, Spain",
                "date": 1295180729967,
                "latitude": 40.4257544,
                "longitude": -3.7043747
            },
            {
                "band": "Doves",
                "location": "Mercury Lounge, New York, NY, US",
                "date": 1297819072954,
                "latitude": 40.7221676,
                "longitude": -73.9867459
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Melkweg Oude Zaal, Amsterdam, Netherlands",
                "date": 1298905996834,
                "latitude": 52.364786,
                "longitude": 4.8812489
            },
            {
                "band": "Dave Matthews Band",
                "location": "Schokoladen, Berlin, Germany",
                "date": 1302178263578,
                "latitude": 52.52977689999999,
                "longitude": 13.3971137
            },
            {
                "band": "Crooked Fingers",
                "location": "Debaser Strand, Stockholm, Sweden",
                "date": 1304648216428,
                "latitude": 59.3146406,
                "longitude": 18.0318701
            },
            {
                "band": "Black Box Recorder",
                "location": "Pacha NYC, New York, NY, US",
                "date": 1308941975767,
                "latitude": 40.763589,
                "longitude": -73.99760630000002
            },
            {
                "band": "Black Veil Brides",
                "location": "Bizz'art, Paris, France",
                "date": 1309087910183,
                "latitude": 48.8790781,
                "longitude": 2.3664324
            },
            {
                "band": "Crooked Fingers",
                "location": "Kawiarnia Naukowa, Krakow, Poland",
                "date": 1310614536247,
                "latitude": 50.040057,
                "longitude": 19.942099
            },
            {
                "band": "Dave Matthews Band",
                "location": "Feierwerk / Orangehouse, Munich, Germany",
                "date": 1312472654713,
                "latitude": 48.1280689,
                "longitude": 11.5351389
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Stork Club, Oakland, CA, US",
                "date": 1312846063832,
                "latitude": 37.8131387,
                "longitude": -122.2683684
            },
            {
                "band": "Daisy Chainsaw",
                "location": "Rodeo Bar, New York, NY, US",
                "date": 1313963131848,
                "latitude": 40.741352,
                "longitude": -73.9810158
            },
            {
                "band": "Black Veil Brides",
                "location": "Paris Las Vegas, Las Vegas, NV, US",
                "date": 1318471813790,
                "latitude": 36.1125414,
                "longitude": -115.1706705
            },
            {
                "band": "Dirty Three",
                "location": "Södra Teaterns Stora Scen, Stockholm, Sweden",
                "date": 1319545138275,
                "latitude": 59.318424,
                "longitude": 18.074412
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Klub RE, Krakow, Poland",
                "date": 1319566214386,
                "latitude": 50.0609973,
                "longitude": 19.9414372
            },
            {
                "band": "David Cook",
                "location": "Circo Volador, Mexico City, Mexico",
                "date": 1321000860639,
                "latitude": 19.4060255,
                "longitude": -99.1256083
            },
            {
                "band": "Broken Social Scene",
                "location": "I.BOAT, Bordeaux, France",
                "date": 1321527702141,
                "latitude": 44.8658875,
                "longitude": -0.5590402999999999
            },
            {
                "band": "Dave Matthews Band",
                "location": "Pianos, New York, NY, US",
                "date": 1321678674731,
                "latitude": 40.721025,
                "longitude": -73.987692
            },
            {
                "band": "Dave Matthews Band",
                "location": "Le Centquatre-Paris (104), Paris, France",
                "date": 1321781264061,
                "latitude": 48.8899821,
                "longitude": 2.3714338
            },
            {
                "band": "8stops7",
                "location": "Hill Country BBQ - Washington, Washington, DC, US",
                "date": 1322407037297,
                "latitude": 38.8951909,
                "longitude": -77.02223359999999
            },
            {
                "band": "Dirty Three",
                "location": "WiZink Center - Palacio de Deportes de la Comunidad de Madrid, Madrid, Spain",
                "date": 1323300296140,
                "latitude": 40.423876,
                "longitude": -3.6717501
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Barboza, Seattle, WA, US",
                "date": 1326228249022,
                "latitude": 47.61381100000001,
                "longitude": -122.319664
            },
            {
                "band": "Crooked Fingers",
                "location": "Trouw, Amsterdam, Netherlands",
                "date": 1326899858702,
                "latitude": 52.35438569999999,
                "longitude": 4.9127382
            },
            {
                "band": "Dirty Three",
                "location": "Nijdrop, Brussels, Belgium",
                "date": 1328885956811,
                "latitude": 50.96736749999999,
                "longitude": 4.1867231
            },
            {
                "band": "Crooked Fingers",
                "location": "Harpa, Reykjavík, Iceland",
                "date": 1329198826369,
                "latitude": 64.1502464,
                "longitude": -21.9322804
            },
            {
                "band": "Doves",
                "location": "La Riviera, Madrid, Spain",
                "date": 1330329907389,
                "latitude": 40.4129999,
                "longitude": -3.7221514
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Scala, London, UK",
                "date": 1332444948144,
                "latitude": 51.5308033,
                "longitude": -0.1208059
            },
            {
                "band": "Black Box Recorder",
                "location": "The Showbox, Seattle, WA, US",
                "date": 1333818729132,
                "latitude": 47.6085565,
                "longitude": -122.3393269
            },
            {
                "band": "Crooked Fingers",
                "location": "The Loft, Atlanta, GA, US",
                "date": 1334714071630,
                "latitude": 33.7920301,
                "longitude": -84.387928
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Teater Pero, Stockholm, Sweden",
                "date": 1334806253705,
                "latitude": 59.3438155,
                "longitude": 18.0560574
            },
            {
                "band": "Black Veil Brides",
                "location": "Zénith de Paris, Paris, France",
                "date": 1335320262635,
                "latitude": 48.89420459999999,
                "longitude": 2.3932285
            },
            {
                "band": "Doves",
                "location": "Le Baiser Salé, Paris, France",
                "date": 1335885438206,
                "latitude": 48.8598659,
                "longitude": 2.347761
            },
            {
                "band": "Dave Matthews Band",
                "location": "Philharmonie De Paris, Paris, France",
                "date": 1335956346491,
                "latitude": 48.8917605,
                "longitude": 2.3941945
            },
            {
                "band": "Dirty Three",
                "location": "Milk, Moscow, Russian Federatio",
                "date": 1339161826147,
                "latitude": 55.755826,
                "longitude": 37.6172999
            },
            {
                "band": "Black Box Recorder",
                "location": "CBGB & OMFUG, New York, NY, US",
                "date": 1341150605357,
                "latitude": 40.725175,
                "longitude": -73.9918817
            },
            {
                "band": "Dirty Three",
                "location": "Webster Hall, New York, NY, US",
                "date": 1341439592763,
                "latitude": 40.7317767,
                "longitude": -73.9891566
            },
            {
                "band": "Crooked Fingers",
                "location": "Kägelbanan Södra Teatern, Stockholm, Sweden",
                "date": 1342003965802,
                "latitude": 59.318424,
                "longitude": 18.074412
            },
            {
                "band": "Escape the Fate",
                "location": "Rockwood Music Hall, New York, NY, US",
                "date": 1342835231763,
                "latitude": 40.7223045,
                "longitude": -73.9885815
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Sala Arena, Madrid, Spain",
                "date": 1344865347173,
                "latitude": 40.42496269999999,
                "longitude": -3.71255
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Horseshoe Tavern, Toronto, ON, Canada",
                "date": 1345754827804,
                "latitude": 43.6490702,
                "longitude": -79.3958951
            },
            {
                "band": "Broken Social Scene",
                "location": "El Corazon, Seattle, WA, US",
                "date": 1346152108581,
                "latitude": 47.6187576,
                "longitude": -122.32934
            },
            {
                "band": "Del Amitri",
                "location": "The Satellite, Los Angeles, CA, US",
                "date": 1350376341715,
                "latitude": 34.089872,
                "longitude": -118.2682879
            },
            {
                "band": "Dave Matthews Band",
                "location": "Royal Circus (Cirque Royal/Koninklijk Circus), Brussels, Belgium",
                "date": 1354594387101,
                "latitude": 50.8488477,
                "longitude": 4.366031599999999
            },
            {
                "band": "Christian Burns",
                "location": "Le Baiser Salé, Paris, France",
                "date": 1356070634332,
                "latitude": 48.8598659,
                "longitude": 2.347761
            },
            {
                "band": "Black Veil Brides",
                "location": "Feierwerk / Kranhalle, Munich, Germany",
                "date": 1357662272252,
                "latitude": 48.1293874,
                "longitude": 11.5336483
            },
            {
                "band": "Doves",
                "location": "Purgatory at The Masquerade, Atlanta, GA, US",
                "date": 1359258175968,
                "latitude": 33.7515657,
                "longitude": -84.3897963
            },
            {
                "band": "Everlast",
                "location": "Rodeo Bar, New York, NY, US",
                "date": 1361989638200,
                "latitude": 40.741352,
                "longitude": -73.9810158
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Melkweg Oude Zaal, Amsterdam, Netherlands",
                "date": 1363383819734,
                "latitude": 52.364786,
                "longitude": 4.8812489
            },
            {
                "band": "Christian Burns",
                "location": "Sunset/Sunside, Paris, France",
                "date": 1366179370759,
                "latitude": 48.8599367,
                "longitude": 2.347751
            },
            {
                "band": "Crooked Fingers",
                "location": "Iceland Airwaves, Reykjavík, Iceland",
                "date": 1366438087592,
                "latitude": 64.1302436,
                "longitude": -21.8715941
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Eddie's Attic, Atlanta, GA, US",
                "date": 1368491100107,
                "latitude": 33.7739746,
                "longitude": -84.2962715
            },
            {
                "band": "Crooked Fingers",
                "location": "Laugardalsholl, Reykjavík, Iceland",
                "date": 1368624373973,
                "latitude": 64.14018519999999,
                "longitude": -21.8767213
            },
            {
                "band": "Black Veil Brides",
                "location": "Cafe Rosenberg, Reykjavík, Iceland",
                "date": 1369408991171,
                "latitude": 64.14613489999999,
                "longitude": -21.9287967
            },
            {
                "band": "Escape the Fate",
                "location": "L'Olympia, Paris, France",
                "date": 1369670486886,
                "latitude": 48.8702193,
                "longitude": 2.3283335
            },
            {
                "band": "Everlast",
                "location": "The Bowery Ballroom, New York, NY, US",
                "date": 1371060489471,
                "latitude": 40.72041249999999,
                "longitude": -73.9933561
            },
            {
                "band": "Black Veil Brides",
                "location": "La Maroquinerie, Paris, France",
                "date": 1371567746449,
                "latitude": 48.8685078,
                "longitude": 2.3920161
            },
            {
                "band": "Doves",
                "location": "City National Grove of Anaheim, Los Angeles, CA, US",
                "date": 1371587614090,
                "latitude": 33.803124,
                "longitude": -117.88594
            },
            {
                "band": "Crooked Fingers",
                "location": "AFAS Live, Amsterdam, Netherlands",
                "date": 1372639378405,
                "latitude": 52.3121046,
                "longitude": 4.9445022
            },
            {
                "band": "Black Box Recorder",
                "location": "The Underworld, London, UK",
                "date": 1373487945615,
                "latitude": 51.5391728,
                "longitude": -0.1420906
            },
            {
                "band": "Crooked Fingers",
                "location": "Tripod, Dublin, Ireland",
                "date": 1375181209597,
                "latitude": 53.3402429,
                "longitude": -6.2608726
            },
            {
                "band": "Alice in Chains",
                "location": "The Opera House, Toronto, ON, Canada",
                "date": 1376543079400,
                "latitude": 43.65900360000001,
                "longitude": -79.3488025
            },
            {
                "band": "Doves",
                "location": "Hallenstadion, Zürich, Switzerland",
                "date": 1378147478296,
                "latitude": 47.4112203,
                "longitude": 8.5516946
            },
            {
                "band": "Broken Social Scene",
                "location": "Olympia Theatre, Dublin, Ireland",
                "date": 1379207286973,
                "latitude": 53.3443127,
                "longitude": -6.2660797
            },
            {
                "band": "Broken Social Scene",
                "location": "Uptown Nightclub, Oakland, CA, US",
                "date": 1379970413832,
                "latitude": 37.8085876,
                "longitude": -122.269635
            },
            {
                "band": "Daisy Chainsaw",
                "location": "The Village, Dublin, Ireland",
                "date": 1380872432103,
                "latitude": 53.3498053,
                "longitude": -6.2603097
            },
            {
                "band": "David Cook",
                "location": "The Sound Room, Oakland, CA, US",
                "date": 1383164145242,
                "latitude": 37.8108665,
                "longitude": -122.2673276
            },
            {
                "band": "Escape the Fate",
                "location": "The Knitting Factory - TriBeCa, New York, NY, US",
                "date": 1383963554257,
                "latitude": 40.7142193,
                "longitude": -73.9558328
            },
            {
                "band": "Dirty Three",
                "location": "Maria am Ufer, Berlin, Germany",
                "date": 1384785337347,
                "latitude": 52.5102954,
                "longitude": 13.4325372
            },
            {
                "band": "Every Avenue",
                "location": "Feierwerk / Orangehouse, Munich, Germany",
                "date": 1386504786239,
                "latitude": 48.1280689,
                "longitude": 11.5351389
            },
            {
                "band": "Christian Burns",
                "location": "Backstage, Munich, Germany",
                "date": 1387458252328,
                "latitude": 48.1451445,
                "longitude": 11.5217969
            },
            {
                "band": "Everlast",
                "location": "Irvine Meadows Amphitheatre, Los Angeles, CA, US",
                "date": 1391746220601,
                "latitude": 34.1794169,
                "longitude": -118.385617
            },
            {
                "band": "Broken Social Scene",
                "location": "KOKO, London, UK",
                "date": 1394119324313,
                "latitude": 51.5347377,
                "longitude": -0.1382573
            },
            {
                "band": "Doves",
                "location": "Ampere, Munich, Germany",
                "date": 1396108611476,
                "latitude": 48.1329363,
                "longitude": 11.5894601
            },
            {
                "band": "Doves",
                "location": "Nectar Lounge, Seattle, WA, US",
                "date": 1402280774586,
                "latitude": 47.6523697,
                "longitude": -122.3538481
            },
            {
                "band": "David Cook",
                "location": "Cavea, Auditorium Parco della Musica, Rome, Italy",
                "date": 1403438277167,
                "latitude": 41.9288906,
                "longitude": 12.4748367
            },
            {
                "band": "Escape the Fate",
                "location": "The Observatory, Los Angeles, CA, US",
                "date": 1405219979663,
                "latitude": 34.1184341,
                "longitude": -118.3003935
            },
            {
                "band": "Dirty Three",
                "location": "Ray Just Arena, Moscow, Russian Federation",
                "date": 1406510472994,
                "latitude": 55.7847281,
                "longitude": 37.5601552
            },
            {
                "band": "8stops7",
                "location": "Klub RE, Krakow, Poland",
                "date": 1407088766986,
                "latitude": 50.0609973,
                "longitude": 19.9414372
            },
            {
                "band": "Broken Social Scene",
                "location": "Academy 2, Dublin, Ireland",
                "date": 1409851062587,
                "latitude": 53.347953,
                "longitude": -6.261985
            },
            {
                "band": "Dirty Three",
                "location": "Init Roma, Rome, Italy",
                "date": 1410439464052,
                "latitude": 41.8866738,
                "longitude": 12.5249096
            },
            {
                "band": "Doves",
                "location": "The Sugar Club, Dublin, Ireland",
                "date": 1410522860425,
                "latitude": 53.3355007,
                "longitude": -6.2566432
            },
            {
                "band": "Broken Social Scene",
                "location": "Nasti, Madrid, Spain",
                "date": 1410827805092,
                "latitude": 40.4257544,
                "longitude": -3.7043747
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Sala El Sol, Madrid, Spain",
                "date": 1411007183442,
                "latitude": 40.4190629,
                "longitude": -3.7016321
            },
            {
                "band": "Black Veil Brides",
                "location": "Fremont Abbey Arts Center, Seattle, WA, US",
                "date": 1411530265247,
                "latitude": 47.6591888,
                "longitude": -122.3496822
            },
            {
                "band": "Doves",
                "location": "The Showbox, Seattle, WA, US",
                "date": 1412685593093,
                "latitude": 47.6085565,
                "longitude": -122.3393269
            },
            {
                "band": "8stops7",
                "location": "Mercury Lounge, New York, NY, US",
                "date": 1413189079265,
                "latitude": 40.7221676,
                "longitude": -73.9867459
            },
            {
                "band": "Christian Burns",
                "location": "Smalls Jazz Club, New York, NY, US",
                "date": 1417020771954,
                "latitude": 40.7343873,
                "longitude": -74.002765
            },
            {
                "band": "8stops7",
                "location": "Nasa, Reykjavík, Iceland",
                "date": 1419211122662,
                "latitude": 64.1474946,
                "longitude": -21.9404268
            },
            {
                "band": "Everlast",
                "location": "Danforth Music Hall, Toronto, ON, Canada",
                "date": 1419399010211,
                "latitude": 43.6762351,
                "longitude": -79.3570038
            },
            {
                "band": "Crooked Fingers",
                "location": "Feierwerk / Orangehouse, Munich, Germany",
                "date": 1420599469750,
                "latitude": 48.1280689,
                "longitude": 11.5351389
            },
            {
                "band": "Doves",
                "location": "The Bell House, New York, NY, US",
                "date": 1421909279300,
                "latitude": 40.67351,
                "longitude": -73.991632
            },
            {
                "band": "Dirty Three",
                "location": "Oakland Metro Operahouse, Oakland, CA, US",
                "date": 1423931930867,
                "latitude": 37.7968292,
                "longitude": -122.2779916
            },
            {
                "band": "Dave Matthews Band",
                "location": "Brooklyn Bowl, New York, NY, US",
                "date": 1425917115701,
                "latitude": 40.7218813,
                "longitude": -73.95739449999999
            },
            {
                "band": "Dave Matthews Band",
                "location": "Gypsy Sally's, Washington, DC, US",
                "date": 1428382227175,
                "latitude": 38.904095,
                "longitude": -77.067877
            },
            {
                "band": "Doves",
                "location": "Stork Club, Oakland, CA, US",
                "date": 1431090336458,
                "latitude": 37.8131387,
                "longitude": -122.2683684
            },
            {
                "band": "8stops7",
                "location": "The Roxy Theatre, Los Angeles, CA, US",
                "date": 1432992326566,
                "latitude": 34.0907813,
                "longitude": -118.388035
            },
            {
                "band": "Black Box Recorder",
                "location": "O2 Shepherd's Bush Empire, London, UK",
                "date": 1435908731262,
                "latitude": 51.5034966,
                "longitude": -0.2242238
            },
            {
                "band": "Everlast",
                "location": "Majestic Ventura Theater, Los Angeles, CA, US",
                "date": 1437742630852,
                "latitude": 34.2804242,
                "longitude": -119.2914618
            },
            {
                "band": "Everlast",
                "location": "The Social, London, UK",
                "date": 1439175559183,
                "latitude": 51.5173006,
                "longitude": -0.1407693
            },
            {
                "band": "Broken Social Scene",
                "location": "Cafe Rosenberg, Reykjavík, Iceland",
                "date": 1439251837870,
                "latitude": 64.14613489999999,
                "longitude": -21.9287967
            },
            {
                "band": "Everlast",
                "location": "Laugardalsholl, Reykjavík, Iceland",
                "date": 1440544141085,
                "latitude": 64.14018519999999,
                "longitude": -21.8767213
            },
            {
                "band": "Black Box Recorder",
                "location": "The Forum, Los Angeles, CA, US",
                "date": 1442971071642,
                "latitude": 33.9582109,
                "longitude": -118.3419015
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Harpa, Reykjavík, Iceland",
                "date": 1443133052460,
                "latitude": 64.1502464,
                "longitude": -21.9322804
            },
            {
                "band": "Escape the Fate",
                "location": "Harpa, Reykjavík, Iceland",
                "date": 1444041652072,
                "latitude": 64.1502464,
                "longitude": -21.9322804
            },
            {
                "band": "Escape the Fate",
                "location": "Propaganda, Moscow, Russian Federation",
                "date": 1445012415619,
                "latitude": 55.7586289,
                "longitude": 37.6328588
            },
            {
                "band": "Black Veil Brides",
                "location": "Academy, Dublin, Ireland",
                "date": 1445478259335,
                "latitude": 53.3480297,
                "longitude": -6.2619981
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Klub Studio, Krakow, Poland",
                "date": 1445958313469,
                "latitude": 50.0680363,
                "longitude": 19.9082967
            },
            {
                "band": "Black Box Recorder",
                "location": "Botanique - Rotonde, Brussels, Belgium",
                "date": 1446702722260,
                "latitude": 50.8546778,
                "longitude": 4.3661713
            },
            {
                "band": "Broken Social Scene",
                "location": "Jazzclub Unterfahrt, Munich, Germany",
                "date": 1452210116783,
                "latitude": 48.1354891,
                "longitude": 11.6005464
            },
            {
                "band": "Broken Social Scene",
                "location": "Feierwerk / Kranhalle, Munich, Germany",
                "date": 1453975751716,
                "latitude": 48.1293874,
                "longitude": 11.5336483
            },
            {
                "band": "Broken Social Scene",
                "location": "The Monarch, London, UK",
                "date": 1454806582024,
                "latitude": 51.5428986,
                "longitude": -0.1484606
            },
            {
                "band": "Crooked Fingers",
                "location": "Nasa, Reykjavík, Iceland",
                "date": 1456143575637,
                "latitude": 64.1474946,
                "longitude": -21.9404268
            },
            {
                "band": "Dirty Three",
                "location": "Proud Camden, London, UK",
                "date": 1456381588881,
                "latitude": 51.5099821,
                "longitude": -0.1180644
            },
            {
                "band": "Doves",
                "location": "Nasa, Reykjavík, Iceland",
                "date": 1456609096207,
                "latitude": 64.1474946,
                "longitude": -21.9404268
            },
            {
                "band": "Dirty Three",
                "location": "Moby Dick Club, Madrid, Spain",
                "date": 1457011475026,
                "latitude": 40.4544885,
                "longitude": -3.6939718
            },
            {
                "band": "8stops7",
                "location": "BT59, Begles, France",
                "date": 1457611633995,
                "latitude": 44.8132603,
                "longitude": -0.5525287
            },
            {
                "band": "Doves",
                "location": "Proud Camden, London, UK",
                "date": 1461226334886,
                "latitude": 51.5099821,
                "longitude": -0.1180644
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Ippodromo Capannelle, Rome, Italy",
                "date": 1461303194002,
                "latitude": 41.8240481,
                "longitude": 12.5694535
            },
            {
                "band": "Christian Burns",
                "location": "Academy 2, Dublin, Ireland",
                "date": 1461382504270,
                "latitude": 53.347953,
                "longitude": -6.261985
            },
            {
                "band": "Crooked Fingers",
                "location": "O2 Forum Kentish Town, London, UK",
                "date": 1461681669157,
                "latitude": 51.55219700000001,
                "longitude": -0.141969
            },
            {
                "band": "8stops7",
                "location": "Vive Cuervo Salón, Mexico City, Mexico",
                "date": 1463104385357,
                "latitude": 19.4432242,
                "longitude": -99.20035469999999
            },
            {
                "band": "Broken Social Scene",
                "location": "The Crocodile, Seattle, WA, US",
                "date": 1463393736997,
                "latitude": 47.6136087,
                "longitude": -122.3442873
            },
            {
                "band": "Dave Matthews Band",
                "location": "Harpa Concert Hall, Reykjavík, Iceland",
                "date": 1463822126413,
                "latitude": 64.1502464,
                "longitude": -21.9322804
            },
            {
                "band": "Doves",
                "location": "High Dive, Seattle, WA, US",
                "date": 1464312017071,
                "latitude": 47.6514909,
                "longitude": -122.351738
            },
            {
                "band": "Doves",
                "location": "Voila, Mexico City, Mexico",
                "date": 1470547592264,
                "latitude": 19.439386,
                "longitude": -99.203794
            },
            {
                "band": "Escape the Fate",
                "location": "O2 Forum Kentish Town, London, UK",
                "date": 1471890025141,
                "latitude": 51.55219700000001,
                "longitude": -0.141969
            },
            {
                "band": "Christian Burns",
                "location": "The Water Rats, London, UK",
                "date": 1472035567725,
                "latitude": 51.5290022,
                "longitude": -0.1195748
            },
            {
                "band": "Dirty Three",
                "location": "U Street Music Hall, Washington, DC, US",
                "date": 1472255758188,
                "latitude": 38.9171386,
                "longitude": -77.0276204
            },
            {
                "band": "Christian Burns",
                "location": "Paradiso Kleine Zaal, Amsterdam, Netherlands",
                "date": 1474504745729,
                "latitude": 52.3621516,
                "longitude": 4.883806499999999
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Annexet, Stockholm, Sweden",
                "date": 1479491749864,
                "latitude": 59.2938854,
                "longitude": 18.0811625
            },
            {
                "band": "Daisy Chainsaw",
                "location": "Oakland City Center, Oakland, CA, US",
                "date": 1480697231178,
                "latitude": 37.8034662,
                "longitude": -122.2716016
            },
            {
                "band": "Black Veil Brides",
                "location": "Le Batofar, Paris, France",
                "date": 1481277528149,
                "latitude": 48.83335,
                "longitude": 2.3793886
            },
            {
                "band": "Broken Social Scene",
                "location": "Le Baiser Salé, Paris, France",
                "date": 1482833687943,
                "latitude": 48.8598659,
                "longitude": 2.347761
            },
            {
                "band": "Crooked Fingers",
                "location": "AFAS Live, Amsterdam, Netherlands",
                "date": 1483136360799,
                "latitude": 52.3121046,
                "longitude": 4.9445022
            },
            {
                "band": "Doves",
                "location": "The Village, Dublin, Ireland",
                "date": 1483530366732,
                "latitude": 53.3498053,
                "longitude": -6.2603097
            },
            {
                "band": "Crooked Fingers",
                "location": "Warehouse, Toronto, ON, Canada",
                "date": 1488336192772,
                "latitude": 43.6501841,
                "longitude": -79.3900334
            },
            {
                "band": "Doves",
                "location": "Hill Country BBQ - New York, New York, NY, US",
                "date": 1489842535382,
                "latitude": 40.7441558,
                "longitude": -73.9904866
            },
            {
                "band": "Dave Matthews Band",
                "location": "Sala Caracol, Madrid, Spain",
                "date": 1491197728618,
                "latitude": 40.4040133,
                "longitude": -3.700034
            },
            {
                "band": "Dave Matthews Band",
                "location": "Iceland Airwaves, Reykjavík, Iceland",
                "date": 1491401617072,
                "latitude": 64.1302436,
                "longitude": -21.8715941
            },
            {
                "band": "Black Veil Brides",
                "location": "Palalottomatica, Rome, Italy",
                "date": 1492147384753,
                "latitude": 41.8252534,
                "longitude": 12.4665412
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Klub RE, Krakow, Poland",
                "date": 1493421624393,
                "latitude": 50.0609973,
                "longitude": 19.9414372
            },
            {
                "band": "Dirty Three",
                "location": "Proud Camden, London, UK",
                "date": 1493683623211,
                "latitude": 51.5099821,
                "longitude": -0.1180644
            },
            {
                "band": "8stops7",
                "location": "Quasimodo, Berlin, Germany",
                "date": 1493721971387,
                "latitude": 52.505874,
                "longitude": 13.3284827
            },
            {
                "band": "Black Box Recorder",
                "location": "Mean Fiddler, London, UK",
                "date": 1496795315111,
                "latitude": 51.5155949,
                "longitude": -0.1302102
            },
            {
                "band": "Black Veil Brides",
                "location": "Scala, London, UK",
                "date": 1500759842839,
                "latitude": 51.5308033,
                "longitude": -0.1208059
            },
            {
                "band": "Alice in Chains",
                "location": "Bitterzoet, Amsterdam, Netherlands",
                "date": 1501688268652,
                "latitude": 52.37739200000001,
                "longitude": 4.89413
            },
            {
                "band": "Broken Social Scene",
                "location": "Nijdrop, Brussels, Belgium",
                "date": 1501922083108,
                "latitude": 50.96736749999999,
                "longitude": 4.1867231
            },
            {
                "band": "Daisy Chainsaw",
                "location": "3Arena, Dublin, Ireland",
                "date": 1502407897272,
                "latitude": 53.3474963,
                "longitude": -6.2285078
            },
            {
                "band": "Black Veil Brides",
                "location": "Chop Suey, Seattle, WA, US",
                "date": 1503323656716,
                "latitude": 47.613694,
                "longitude": -122.3144219
            },
            {
                "band": "Black Box Recorder",
                "location": "Teater Pero, Stockholm, Sweden",
                "date": 1503817777726,
                "latitude": 59.3438155,
                "longitude": 18.0560574
            },
            {
                "band": "Everlast",
                "location": "Auditorium Parco della Musica, Rome, Italy",
                "date": 1506382501526,
                "latitude": 41.9288906,
                "longitude": 12.4748367
            },
            {
                "band": "Doves",
                "location": "La Flèche d'Or, Paris, France",
                "date": 1507276469175,
                "latitude": 48.859433,
                "longitude": 2.402906
            },
            {
                "band": "8stops7",
                "location": "Paradiso, Amsterdam, Netherlands",
                "date": 1507538902764,
                "latitude": 52.3621516,
                "longitude": 4.883806499999999
            },
            {
                "band": "Escape the Fate",
                "location": "Tochka Club, Moscow, Russian Federation",
                "date": 1508191888631,
                "latitude": 55.72758589999999,
                "longitude": 37.6060215
            },
            {
                "band": "Everlast",
                "location": "The Workman's Club, Dublin, Ireland",
                "date": 1508487561597,
                "latitude": 53.3453465,
                "longitude": -6.2664073
            },
            {
                "band": "Dave Matthews Band",
                "location": "Feierwerk / Hansa 39, Munich, Germany",
                "date": 1509206120968,
                "latitude": 48.1291971,
                "longitude": 11.533535
            },
            {
                "band": "Black Box Recorder",
                "location": "Neumos Crystal Ball Reading Room, Seattle, WA, US",
                "date": 1510960965545,
                "latitude": 47.613938,
                "longitude": -122.3196199
            },
            {
                "band": "Del Amitri",
                "location": "Birdland, New York, NY, US",
                "date": 1513579664582,
                "latitude": 40.7589663,
                "longitude": -73.98961729999999
            },
            {
                "band": "Dave Matthews Band",
                "location": "Disco Volante, Oakland, CA, US",
                "date": 1516982387514,
                "latitude": 37.8031731,
                "longitude": -122.2686268
            },
            {
                "band": "Escape the Fate",
                "location": "Zénith de Limoges, Limoges, France",
                "date": 1517599690877,
                "latitude": 45.8622864,
                "longitude": 1.27935
            },
            {
                "band": "Del Amitri",
                "location": "Knitting Factory, New York, NY, US",
                "date": 1522717314580,
                "latitude": 40.7142193,
                "longitude": -73.9558328
            },
            {
                "band": "Black Veil Brides",
                "location": "Birdland, New York, NY, US",
                "date": 1524277300728,
                "latitude": 40.7589663,
                "longitude": -73.98961729999999
            },
            {
                "band": "Crooked Fingers",
                "location": "Ampere, Munich, Germany",
                "date": 1524600004428,
                "latitude": 48.1329363,
                "longitude": 11.5894601
            },
            {
                "band": "Del Amitri",
                "location": "Bi Nuu, Berlin, Germany",
                "date": 1524774411261,
                "latitude": 52.500711,
                "longitude": 13.441273
            },
            {
                "band": "Crooked Fingers",
                "location": "Centre Culturel Jean Gagnant, Limoges, France",
                "date": 1525145746776,
                "latitude": 45.8782298,
                "longitude": 1.2891542
            },
            {
                "band": "David Cook",
                "location": "U Street Music Hall, Washington, DC, US",
                "date": 1528355124645,
                "latitude": 38.9171386,
                "longitude": -77.0276204
            },
            {
                "band": "Black Veil Brides",
                "location": "Club Anton, Oakland, CA, US",
                "date": 1528551534001,
                "latitude": 37.7967999,
                "longitude": -122.2754412
            },
            {
                "band": "Dave Matthews Band",
                "location": "Atlantico, Rome, Italy",
                "date": 1529352860027,
                "latitude": 41.8171893,
                "longitude": 12.4629842
            },
            {
                "band": "Crooked Fingers",
                "location": "Kwadrat, Krakow, Poland",
                "date": 1531290583692,
                "latitude": 50.0838472,
                "longitude": 19.9961303
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Annexet, Stockholm, Sweden",
                "date": 1531436854569,
                "latitude": 59.2938854,
                "longitude": 18.0811625
            },
            {
                "band": "Alice in Chains",
                "location": "Sala Arena, Madrid, Spain",
                "date": 1535699656923,
                "latitude": 40.42496269999999,
                "longitude": -3.71255
            },
            {
                "band": "Doves",
                "location": "The Observatory, Los Angeles, CA, US",
                "date": 1536197382713,
                "latitude": 34.1184341,
                "longitude": -118.3003935
            },
            {
                "band": "Crooked Fingers",
                "location": "Melkweg Oude Zaal, Amsterdam, Netherlands",
                "date": 1540997040316,
                "latitude": 52.364786,
                "longitude": 4.8812489
            },
            {
                "band": "David Cook",
                "location": "Exil, Zürich, Switzerland",
                "date": 1543311529149,
                "latitude": 47.38830369999999,
                "longitude": 8.5193197
            },
            {
                "band": "Broken Social Scene",
                "location": "Proud Camden, London, UK",
                "date": 1543566345212,
                "latitude": 51.5099821,
                "longitude": -0.1180644
            },
            {
                "band": "Broken Social Scene",
                "location": "Kaufleuten, Zürich, Switzerland",
                "date": 1545999140457,
                "latitude": 47.3716391,
                "longitude": 8.5363625
            },
            {
                "band": "Black Veil Brides",
                "location": "229, London, UK",
                "date": 1546697431715,
                "latitude": 51.52365469999999,
                "longitude": -0.144551
            },
            {
                "band": "Dirty Three",
                "location": "Bercy Arena, Paris, France",
                "date": 1546748976609,
                "latitude": 48.8385379,
                "longitude": 2.3785842
            },
            {
                "band": "Black Box Recorder",
                "location": "Black Cat, Washington, DC, US",
                "date": 1546848761134,
                "latitude": 38.9145825,
                "longitude": -77.0317283
            },
            {
                "band": "Crooked Fingers",
                "location": "Cafe Rosenberg, Reykjavík, Iceland",
                "date": 1550850763970,
                "latitude": 64.14613489999999,
                "longitude": -21.9287967
            },
            {
                "band": "8stops7",
                "location": "Maria am Ufer, Berlin, Germany",
                "date": 1553687612577,
                "latitude": 52.5102954,
                "longitude": 13.4325372
            },
            {
                "band": "Daisy Chainsaw",
                "location": "El Cid, Los Angeles, CA, US",
                "date": 1555784135236,
                "latitude": 34.093836,
                "longitude": -118.2824171
            },
            {
                "band": "Doves",
                "location": "The Underworld, London, UK",
                "date": 1556233897194,
                "latitude": 51.5391728,
                "longitude": -0.1420906
            },
            {
                "band": "Christian Burns",
                "location": "Feierwerk / Hansa 39, Munich, Germany",
                "date": 1557541880548,
                "latitude": 48.1291971,
                "longitude": 11.533535
            },
            {
                "band": "Doves",
                "location": "Nouveau Casino, Paris, France",
                "date": 1558664416219,
                "latitude": 48.86587129999999,
                "longitude": 2.3778975
            },
            {
                "band": "Daisy Chainsaw",
                "location": "El Lokal, Zürich, Switzerland",
                "date": 1560153454581,
                "latitude": 47.374641,
                "longitude": 8.533891299999999
            },
            {
                "band": "Broken Social Scene",
                "location": "Mama Buzz Cafe, Oakland, CA, US",
                "date": 1560639521442,
                "latitude": 37.8043514,
                "longitude": -122.2711639
            },
            {
                "band": "Dave Matthews Band",
                "location": "The Lexington, London, UK",
                "date": 1561025717107,
                "latitude": 51.5317299,
                "longitude": -0.1114164
            },
            {
                "band": "Doves",
                "location": "Hill Country BBQ - Washington, Washington, DC, US",
                "date": 1561323592671,
                "latitude": 38.8951909,
                "longitude": -77.02223359999999
            },
            {
                "band": "8stops7",
                "location": "Atlanta Room, Smith's Olde Bar, Atlanta, GA, US",
                "date": 1561776085106,
                "latitude": 33.797612,
                "longitude": -84.36885199999999
            },
            {
                "band": "Crooked Fingers",
                "location": "Columbiahalle, Berlin, Germany",
                "date": 1563204132207,
                "latitude": 52.4845106,
                "longitude": 13.392469
            },
            {
                "band": "Everlast",
                "location": "The Masquerade, Atlanta, GA, US",
                "date": 1563348261324,
                "latitude": 33.7515657,
                "longitude": -84.3897963
            },
            {
                "band": "8stops7",
                "location": "NYCB Theatre at Westbury, New York, NY, US",
                "date": 1563473046623,
                "latitude": 40.7733696,
                "longitude": -73.5589498
            },
            {
                "band": "Christian Burns",
                "location": "Kawiarnia Naukowa, Krakow, Poland",
                "date": 1563704502595,
                "latitude": 50.040057,
                "longitude": 19.942099
            },
            {
                "band": "Daisy Chainsaw",
                "location": "Paradiso, Amsterdam, Netherlands",
                "date": 1564574693017,
                "latitude": 52.3621516,
                "longitude": 4.883806499999999
            },
            {
                "band": "Dave Matthews Band",
                "location": "Lido, Berlin, Germany",
                "date": 1564871784389,
                "latitude": 52.49917,
                "longitude": 13.445
            },
            {
                "band": "Dirty Three",
                "location": "Médoquine, Talence, France",
                "date": 1567217463772,
                "latitude": 44.8189984,
                "longitude": -0.5982263999999999
            },
            {
                "band": "Everlast",
                "location": "Iridium, New York, NY, US",
                "date": 1567676415255,
                "latitude": 40.761896,
                "longitude": -73.983525
            },
            {
                "band": "Del Amitri",
                "location": "Sound Academy, Toronto, ON, Canada",
                "date": 1569126340647,
                "latitude": 43.63967479999999,
                "longitude": -79.3535794
            },
            {
                "band": "Escape the Fate",
                "location": "Moby Dick Club, Madrid, Spain",
                "date": 1569378340619,
                "latitude": 40.4544885,
                "longitude": -3.6939718
            },
            {
                "band": "Black Rebel Motorcycle Club",
                "location": "Nijdrop, Brussels, Belgium",
                "date": 1572310318590,
                "latitude": 50.96736749999999,
                "longitude": 4.1867231
            },
            {
                "band": "Doves",
                "location": "The Rockpile West, Toronto, ON, Canada",
                "date": 1572730844395,
                "latitude": 43.6294203,
                "longitude": -79.5489308
            },
            {
                "band": "Dave Matthews Band",
                "location": "The Workman's Club, Dublin, Ireland",
                "date": 1573993903345,
                "latitude": 53.3453465,
                "longitude": -6.2664073
            }
        ]
    });
});

// Error case
it('search with no auth', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/search')
    .withQueryParams('bandIds', '[857,3]')
    .expectStatus(403);
});

it('error lattidude required', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/search')
    .withHeaders('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQyLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDE4OTY0NTh9.Fm03RASssKqS8ConHP8uh7bUXsESxZPaXusQRX28k3Y')
    .expectStatus(400)
    .expectJson({
      "status": "error",
      "data": [
          {
              "message": "\"latitude\" is required",
              "path": [
                  "latitude"
              ]
          }
      ]
  });
});

it('error lattidude required', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/search')
    .withHeaders('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQyLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDE4OTY0NTh9.Fm03RASssKqS8ConHP8uh7bUXsESxZPaXusQRX28k3Y')
    .expectStatus(400)
    .withQueryParams('latitude','48.8814422')
    .expectJson({
      "status": "error",
      "data": [
          {
              "message": "\"longitude\" is required",
              "path": [
                  "longitude"
              ]
          }
      ]
  });
});

it('error radius required', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/search')
    .withHeaders('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQyLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDE4OTY0NTh9.Fm03RASssKqS8ConHP8uh7bUXsESxZPaXusQRX28k3Y')
    .expectStatus(400)
    .withQueryParams('latitude','48.8814422')
    .withQueryParams('longitude','2.3684356')
    .expectJson({
      "status": "error",
      "data": [
          {
              "message": "\"radius\" is required",
              "path": [
                  "radius"
              ]
          }
      ]
  });
});

it('error radius is integer', async () => {
  await pactum.spec()
    .get('http://localhost:8082/api/search')
    .withHeaders('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQyLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDE4OTY0NTh9.Fm03RASssKqS8ConHP8uh7bUXsESxZPaXusQRX28k3Y')
    .expectStatus(400)
    .withQueryParams('latitude','48.8814422')
    .withQueryParams('longitude','2.3684356')
    .withQueryParams('radius','10.2345')
    .expectJson({
      "status": "error",
      "data": [
          {
            "message": "\"radius\" must be an integer",
            "path": [
                "radius"
            ]
          }
      ]
  });
});
