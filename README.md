# Test Technique WeMaintain

## Step 1

 ### A) To initialize the project without docker:

Clone the project from the repo 
use this commande : 

    - npm install
    - npm start

It's launched.

 ### B) To initialize the project with docker:

I added a docker part if you want you can create an image with the script buildAndPush.sh
    
    - expected param : ./buildAndPush.sh <BRANCH> <REPO>:<BRANCH>-v<NUM>.<NUM>.<NUM>
    - example : ./buildAndPush.sh dev proxy:dev-v0.1.26

You also need to change the repository and the project you target
    
    - repository="registry.gitlab.com/moknineb"
    - project="wemaintaintesttech/"

After that you can run your project with a docker-compose. All you need is to change the image you pull :
    
    - example : image: registry.gitlab.com/moknineb/wemaintaintesttech/wemaintain:local-v0.0.1

You can also launch the different tests with the command "npm test" I use mocha and pactum to manage them.

Improvement point : 
    
    - Change the dataset for the tests to avoid checking too much data

NB: I added an authentication on the GET api/search endpoint. Here is an example of a Token Bearer :

    - Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQyLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDE4OTY0NTh9.Fm03RASssKqS8ConHP8uh7bUXsESxZPaXusQRX28k3Y 


## Step 2 : 

To move to an architecture that can support 2 million bands, 10,000 concerts halls and 200 million concerts. I recommend setting up a MySQL DB with the implementation of indexes on the different tables. We can also put the different containers under a kubernetes cluster to duplicate the different server(back, front) which will avoid bottlenecks in input and drastically reduce the possible down time.


### Other question :

What do you identify as possible risks in the architecture that you described in the long run, and how would you mitigate those?

    On the long term the risk remains the fact of reaching the threshold or the number of data is too high. To reduce this risk one of the possibilities is to set up a shard or partition system to regulate the size of some data tables. 

What are the key elements of your architecture that would need to be monitored, and what would you use to monitor those?

    The different elements to monitor are :

        - The average response time and the maximum response time of the requests
        - The execution time of the different requests in the database
            - To do this we can use Jeager or ElasticSearch which will keep track of each request with their different execution times (so it is possible to see also the response time of the database)
        - Finally the CPU consumption of the different servers
            - To do this we can use the Kubernetes administration panel
