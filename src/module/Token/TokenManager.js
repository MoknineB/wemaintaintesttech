import { verify } from "./jwt.js";
import config from "../../config/index.js";


export default class TokenManager {
  /**
   * check token validity
   *
   * @param token
   * @returns {*}
   */
  static async checkToken(token) {
    const decoded = await verify(token, config.jwtSecret, {
      clockTolerance: 10
    });
    return {
      success: true,
      decoded: decoded
    };
  }
}
