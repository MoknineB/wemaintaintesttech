import jwt from "jsonwebtoken";

/**
 * create a jwt with secret as signature
 *
 * @static
 * @param {*} info
 * @param {*} secret
 * @returns jwt
 */
export const sign = (info, secret, options = null) => {
  return new Promise((resolve, reject) =>
    jwt.sign(info, secret, options, (err, token) => {
      if (err) {
        return reject(err);
      }
      return resolve(token);
    })
  );
};

/**
 * get info inside the jwt but does not tell if token is still good or not
 *
 * @static
 * @param {*} token
 * @returns object inside the jwt
 */
export const decode = token => {
  return jwt.decode(token);
};

/**
 * (asynchronous) check token validity and get info inside
 *
 * @static
 * @param {*} token
 * @param {*} secret
 * @returns promise with decoded state (err or decoded token)
 */
export const verify = (token, secret) => {
  return new Promise((resolve, reject) =>
    jwt.verify(token, secret, (err, decoded) => {
      if (err) {
        return reject(err);
      }
      return resolve(decoded);
    })
  );
};
