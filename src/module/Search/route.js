import express from "express";
import asyncHandler from "../../utils/asyncHandler.js";
import authenticate from "../../utils/authenticate.js";
import { getSearch } from "./search.js";

let router = express.Router();

router.get(
  "/",
  authenticate,
  asyncHandler(async (req, res) => {
    const searchQuery = req.query;
    const result = await getSearch(searchQuery);

    res.json(result);
  })
);

export default router;
