import Validator from "../../Validator/index.js";
import Joi from "joi";

class SearchValidator extends Validator {
  constructor(data) {
    super();
    this.schema = Joi.object({
      bandIds: Joi.array(),
      latitude: Joi.when("bandIds", {is: Joi.exist(), then: Joi.number().precision(7), otherwise : Joi.number().precision(7).exist()}),
      longitude: Joi.when("bandIds", {is: Joi.exist(), then: Joi.number().precision(7), otherwise : Joi.number().precision(7).exist()}),
      radius: Joi.when("bandIds", {is: Joi.exist(), then: Joi.number().integer(), otherwise : Joi.number().integer().exist()})
    })
    this.data = data;
  }
}
export default SearchValidator;
