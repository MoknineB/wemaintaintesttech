import SearchValidator  from "./Validator/SearchValidator.js";
import { isPointWithinRadius } from 'geolib';
import concertsJson from "../DB/concerts.json"
import bandsJson from "../DB/bands.json"
import venuesJson from"../DB/venues.json"
const convertionKmM = 1000;

/**
 * Main method of the Search service
 * takes the queries passed in the request and returns concerts
 * @param {*} searchQuery 
 * @returns {}
 * format example:
 * {
        "band": "Radiohead",
        "location": "Point Ephémère, Paris, France",
        "date": 1569531810650,
        "latitude": 48.8814422,
        "longitude": 2.3684356
    }
 */
export const getSearch = async searchQuery => {
  
  /**
   * Data validation 
   */
  const searchValidator = new SearchValidator(searchQuery);
  await searchValidator.validate();

  let concertFound = [];
  let venuesFound = [];
  let dataFound = [];

  let hasBandIds = searchQuery.hasOwnProperty("bandIds");
  let hasRadusFilter = searchQuery.hasOwnProperty("latitude") && searchQuery.hasOwnProperty("longitude") && searchQuery.hasOwnProperty("radius");
  let bandIds= searchQuery.hasOwnProperty("bandIds") ? JSON.parse(searchQuery.bandIds) : [];
  let latitude = searchQuery.latitude;
  let longitude = searchQuery.longitude;
  let radius = searchQuery.radius;
  
  if (hasBandIds) {
    concertFound = findConcertByIds(bandIds);
    dataFound = consolidateConcerts(concertFound);
    if (hasRadusFilter) {
      dataFound = filterRadius(dataFound, latitude, longitude, radius);
    }
  } else {
    venuesFound = filterRadius(venuesJson, latitude, longitude, radius);
    dataFound = filterAndConsolidateConcertsFromVenues(venuesFound);
  }

  //Function that allows you to sort in ascending order
  dataFound.sort(function (a, b) {
    return a.date - b.date;
  });

  return {
    success: true,
    result : dataFound
  };
};

/**
 * Method to find the list of concerts with an id array
 * @param {*} bandIds 
 * @returns {[]}
 */
function findConcertByIds(bandIds) {
  let concertFound = [];
  bandIds.forEach(id => {
    let concertFromId = concertsJson.filter(concert => { return concert.bandId == id; });
    concertFound = concertFound.concat(concertFromId);
  });
  return concertFound;
}

/**
 * Method that takes a concert array, consolidates and formats the data with bands and venues
 * @param {*} concertFound 
 * @returns {}
 * format : {
        "band": "Radiohead",
        "location": "Point Ephémère, Paris, France",
        "date": 1569531810650,
        "latitude": 48.8814422,
        "longitude": 2.3684356
    }
 */
function consolidateConcerts(concertFound) {
  let tempConcertFound = [];
  tempConcertFound = concertFound.map(concert => {
    let bandFound = bandsJson.find(band => band.id == concert.bandId);
    let venueFound = venuesJson.find(venue => venue.id == concert.venueId);
    return {
      band: bandFound.name,
      location: venueFound.name,
      date: concert.date,
      latitude: venueFound.latitude,
      longitude: venueFound.longitude
    };
  });
  return tempConcertFound;
}

/**
 *  Method of filtering concerts by looking if they are in the requested area
 * @param {*} filterConcertFound 
 * @param {*} latitude 
 * @param {*} longitude 
 * @param {*} radius 
 * @returns {}
 */
 function filterRadius(filterConcertFound, latitude, longitude, radius) {
  filterConcertFound = filterConcertFound.filter(concert => {
    return isPointWithinRadius(
      { latitude, longitude },
      { latitude: concert.latitude, longitude: concert.longitude },
      radius * convertionKmM
    );
  });
  return filterConcertFound;
}

/**
 * Method that takes an array of venues, filters, consolidates and formats the data with bands and venues
 * @param {*} venuesFoundInRadius 
 * @returns 
 * format : {
      "band": "Radiohead",
      "location": "Point Ephémère, Paris, France",
      "date": 1569531810650,
      "latitude": 48.8814422,
      "longitude": 2.3684356
  }
 */
function filterAndConsolidateConcertsFromVenues(venuesFoundInRadius) {
  let tempConcertFound = []
  venuesFoundInRadius.forEach(venue => {
    let concertFromId = concertsJson.filter(concert => { return concert.bandId == venue.id; });
    tempConcertFound = tempConcertFound.concat(concertFromId);
  });
  return consolidateConcerts(tempConcertFound);
}

