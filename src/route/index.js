import bodyParser from 'body-parser';
import search from '../module/Search/index.js';

const preMiddleware = app => {
	app.disable('x-powered-by');
	app.enable('trust proxy');
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));

	return app;
};

const init = app => {
	//List of endPoint
	app.use('/api/search', search.route);
	app.get('/api/ping', (req, res) => {
		res.json({ success: true })
	});

	return app;
};

const responseMiddleware = app => {
	app.use((err, req, res, next) => {
		console.log(err);

		let responseMessage = {
			status: 'error',
			data: {},
		};

		let statusCode = 400;
		responseMessage.data = JSON.parse(JSON.stringify(err.ResponseMessage.data));

		try {
			res.status(statusCode).send(responseMessage);
		} catch (e) {
			throw e;
		}
	});
	return app;
};

export default {
	preMiddleware,
	init,
	responseMiddleware,
};
