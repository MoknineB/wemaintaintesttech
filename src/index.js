import express from "express";
import route from "./route/index.js";
import parseError from "parse-error";
import http from "http";
import{
  middleware,
} from "@banzaicloud/service-tools";
import stoppable from "stoppable";
import dotenv from "dotenv";
dotenv.config()

const app = express();
route.preMiddleware(app);
route.init(app);
route.responseMiddleware(app);

if (typeof PhusionPassenger != "undefined") {
  app.set("port", "passenger");
} else {
  app.set("port", 8082);
}
app.use(middleware.express.errorHandler());
const server = stoppable(http.createServer(app));
server.listen(8082);

server.once("listening", () => {
  const { port } = server.address();
  console.log(`server is listening on port ${port}`);
});

server.once("error", err => {
  console.error("server error", err);
  process.exit(1);
});

process.on("uncaughtException", e => {
  var data = parseError(e);
  //todo send slack notif or email
  //sendSlackNotif(data, "uncaughtException");
  console.log(data);
  process.exit(1);
});

export default app;
