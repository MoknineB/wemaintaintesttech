import { TokenManager } from "../module/Token/index.js";

export default (req, res, next) => {
  const authorizationHeader = req.headers.authorization;
  let token;

  if (authorizationHeader) {
    token = authorizationHeader.split(" ")[1];
  }
  if (token) {
    TokenManager.checkToken(token)
      .then(tokenIsValid => {
        if (tokenIsValid.success) {
          let { decoded } = tokenIsValid;
          let user = {};
          user.id = decoded.id;
          user.email = decoded.username;
          user.sessionId = decoded.sessionId;
          user.group_id = decoded.group_id;
          user.group_name = decoded.group_name;
          req.currentUser = user;
          return next();
        } else {
          res.status(401).json({ error: "Failed to authenticate" });
        }
      })
      .catch(err => {
        console.log(err);
        res.status(401).json({ error: "Failed to authenticate" });
      });
  } else {
    res.status(403).json({
      error: "No token provided"
    });
  }
};
