#!/bin/bash

usage() {
    echo "$0 <env> <image>"
    echo ""
    echo "env   : dev, local or prod"
    echo "image : name of the image"
    echo ""
    echo "Version 0.1"
    exit 0
}

repository="registry.gitlab.com/moknineb"
project="wemaintaintesttech/"
env="$1"
image="$2"
dockerfile=""

if test "$#" -ne 2;then
    usage
fi

case $env in
    'local')
        dockerfile="Dockerfile.local"
    ;;
    'prod')
        dockerfile="Dockerfile.prod"
    ;;
    'dev')
        dockerfile="Dockerfile.dev"
    ;;
    *)
        echo "bad env"
        exit 1
    ;;
esac

docker build -t "$image" -f "$dockerfile" .
docker tag "$image" "$repository/${project}${image}"
docker push "$repository/${project}${image}"